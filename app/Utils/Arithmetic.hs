module Utils.Arithmetic where


average :: Integral a => [a] -> a
average x = sum x `div` (fromIntegral.length) x
