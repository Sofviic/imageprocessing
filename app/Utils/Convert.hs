module Utils.Convert where

import qualified Codec.Picture as J
import Types

undyn :: J.DynamicImage -> Maybe (J.Image J.PixelRGBA8)
undyn (J.ImageRGBA8 img) = Just img
undyn _                  = Nothing

tuplify :: J.PixelRGBA8 -> Colour
tuplify (J.PixelRGBA8 r g b a) = (fromEnum r,fromEnum g,fromEnum b,fromEnum a)
untuplify :: Colour -> J.PixelRGBA8
untuplify (r,g,b,a) = (J.PixelRGBA8 (toEnum r) (toEnum g) (toEnum b) (toEnum a))
