module Processing where

import Control.Monad
import Control.Comonad

import Types
import Utils.Tuple
import Utils.Arithmetic

processing :: Image Colour -> Image Colour
processing = img_unalpha . img_clamp . img_edge 2

-----
img_unalpha :: Image Colour -> Image Colour
img_unalpha = fmap $ \(r,g,b,_) -> (r,g,b,255)

img_clamp :: Image Colour -> Image Colour
img_clamp = fmap $ map4a ((max 0) . (min 255))

img_invert :: Image Colour -> Image Colour
img_invert = fmap $ \(r,g,b,a) -> (255-r,255-g,255-b,a)

img_flip ::Image Colour -> Image Colour
img_flip = extend (flip ($ 0) 0 . getImage)

img_blur :: Int -> Image Colour -> Image Colour
img_blur = img_conv conv_avg

img_edge :: Int -> Image Colour -> Image Colour
img_edge = img_conv conv_edge

img_sharp :: Int -> Image Colour -> Image Colour
img_sharp = img_conv conv_sharp

img_conv :: (Int -> Int -> Int -> (Int -> Int -> Colour) -> Colour) -> Int -> Image Colour -> Image Colour
img_conv conv r = img_flip . extend (conv r 0 0 . getImage)

-----
convolution :: Int -> (Int -> Int -> Colour -> Colour) -> Int -> Int -> (Int -> Int -> Colour) -> Colour
convolution r kernel x y f = map4a sum $ sequence4 [kernel i j (f (x+i) (y+j)) | i <- [-r..r], j <- [-r..r]]

conv_id :: Int -> Int -> Int -> (Int -> Int -> Colour) -> Colour
conv_id r = convolution r (\i j -> map4a $ if (i,j) == (0,0) then (*1) else (*0))

conv_edge :: Int -> Int -> Int -> (Int -> Int -> Colour) -> Colour
conv_edge r = convolution r (\i j -> map4a $ if (i,j) == (0,0) then (*((2*r+1)^2 - 1)) else (*(-1)))

conv_sharp :: Int -> Int -> Int -> (Int -> Int -> Colour) -> Colour
conv_sharp r = convolution r (\i j -> map4a $ if (i,j) == (0,0) then (*(2*r+1)^2) else (*(-1)))

conv_avg :: Int -> Int -> Int -> (Int -> Int -> Colour) -> Colour
conv_avg r = convolution r (\_ _ -> map4a (`div`(2*r+1)^2))

