module PNGIO where

--deprecated

import qualified Codec.Compression.Zlib as Z
import qualified Codec.Picture as J
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as B
import qualified Data.Vector.Storable as V
import Data.Array
import Data.Bits
import Data.List
import Data.Word
import Types

readPNG :: FilePath -> IO (Maybe (Image Colour))
readPNG fp = do
  vps <- readFile fp
  return $ case J.decodePng $ BC.pack vps of
             Left err -> Nothing
             Right img -> Just $ undefined

writePNG :: FilePath -> Int -> Int -> Int -> Int -> Image Colour -> IO ()
writePNG fp x0 x1 y0 y1 i = B.writeFile fp . png $ getSImg x0 x1 y0 y1 i

getSImg :: Int -> Int -> Int -> Int -> Image a -> [[a]]
getSImg x0 x1 y0 y1 i = do
  row <- [y0..y1]
  return $ do
    col <- [x0..x1]
    return $ getImage i row col
    

getList :: Int -> Int -> (Int -> a) -> [a]
getList a b f = fmap f [a..b]

getTImg :: a -> Int -> Int -> [[a]] -> Image a
getTImg e x0 y0 l = Image $ \x y -> case l !? (y-y0) >>= (!? (x-x0)) of
                                      Nothing -> e
                                      Just x -> x


(!?) :: [a] -> Int -> Maybe a
l !? i = if i > 0 && i < length l
         then Just (l !! i)
         else Nothing

infixr 8 .#.
infixr 8 .##.
infixr 8 .###.
infixr 8 .####.
infixr 8 .#####.
(.#.) = (.)
(.##.) f = ((f .) .)
(.###.) f = (((f .) .) .)
(.####.) f = ((((f .) .) .) .)
(.#####.) f = (((((f .) .) .) .) .)

------------------------------------
be8 :: Word8 -> B.ByteString
be8 x = B.singleton x

be32 :: Word32 -> B.ByteString
be32 x = B.pack [fromIntegral (x `shiftR` sh) | sh <- [24,16,8,0]]

pack :: String -> B.ByteString
pack xs = B.pack $ map (fromIntegral.fromEnum) xs

unpack :: B.ByteString -> String
unpack xs = map (toEnum.fromIntegral) (B.unpack xs)

hdr, iHDR, iDAT, iEND :: B.ByteString
hdr = pack "\137\80\78\71\13\10\26\10"
iHDR = pack "IHDR"
iDAT = pack "IDAT"
iEND = pack "IEND"

chunk :: B.ByteString -> B.ByteString -> [B.ByteString]
chunk tag xs = [be32 (fromIntegral $ B.length xs), dat, be32 (crc dat)]
    where dat = B.append tag xs

crc :: B.ByteString -> Word32
crc xs = updateCrc 0xffffffff xs `xor` 0xffffffff

updateCrc :: Word32 -> B.ByteString -> Word32
updateCrc = B.foldl' crcStep

crcStep :: Word32 -> Word8 -> Word32
crcStep crc ch = (crcTab ! n) `xor` (crc `shiftR` 8)
    where n = fromIntegral (crc `xor` fromIntegral ch)

crcTab :: Array Word8 Word32
crcTab = listArray (0,255) $ flip map [0..255] (\n ->
    foldl' (\c k -> if c .&. 1 == 1
                      then 0xedb88320 `xor` (c `shiftR` 1)
                      else c `shiftR` 1) n [0..7])

png :: [[(Int,Int,Int,Int)]] -> B.ByteString
png dat = B.concat $ hdr : concat [ihdr, imgdat ,iend]
     where height = fromIntegral $ length dat
           width = fromIntegral $ length (head dat)
           ihdr = chunk iHDR $ B.concat 
                     [ be32 height
                     , be32 width
                     , be8 8   -- bits per sample (8 for r, 8 for g, 8 for b, 8 for a)
                     , be8 6   -- color type (2=rgb) | (4=a)
                     , be8 0   -- compression method
                     , be8 0   -- filter method
                     , be8 0 ] -- interlace method
           imgdat = chunk iDAT (Z.compress imagedata)
           imagedata = B.concat $ map scanline dat
           iend = chunk iEND B.empty
 
scanline :: [(Int,Int,Int,Int)] -> B.ByteString
scanline dat = B.pack (0 : (map fromIntegral $ concatMap (\(r,g,b,a) -> [r,g,b,a]) dat))
