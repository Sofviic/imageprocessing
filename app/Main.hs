module Main where

import Control.Monad
import System.Environment
import System.FilePath
import qualified Codec.Picture as J

import Processing
import Types
import Utils.Compose
import Utils.Convert

-- TODO use (MayebT . IO) instead of IO.
main :: IO ()
main = do
        args <- getArgs
        guard $ length args > 0
        let fp = args !! 0
        let filename = takeFileName fp
        putStrLn $ "reading Image: " ++ filename
        putStrLn $ "path: " ++ fp
        imdread <- J.readImage fp
        case imdread of
            Left  err -> putStrLn $ "Error: " ++ err
            Right imd -> do
                    putStrLn $ "Image read."
                    case undyn imd of
                        Nothing -> putStrLn $ "Error: RGBA8 images only."
                        Just imj -> do
                                let width = J.imageWidth imj
                                let height = J.imageHeight imj
                                let img = Image $ \x y -> 
                                        tuplify $ J.pixelAt imj (x `mod` width) (y `mod` height)
                                putStrLn $ "Processing..."
                                let oimg = processing img
                                putStrLn $ "Processing done."
                                putStrLn $ "Writing..."
                                J.writePng "src/output.png" 
                                        $ J.generateImage (untuplify .#. getImage oimg) width height
                                putStrLn $ "Writing done."
        putStrLn $ "Done."

