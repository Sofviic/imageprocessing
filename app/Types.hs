module Types where

import Utils.Compose
import Control.Monad
import Control.Comonad

data Image a = Image { getImage :: Int -> Int -> a }
type Colour = (Int,Int,Int,Int)

instance Functor Image where
    fmap f = Image . f .##. getImage

instance Applicative Image where
    pure = Image . const . const
    liftA2 f imga imgb = Image $ \x y -> f (getImage imga x y) (getImage imgb x y)

-- Unpointed Finite
instance Monad Image where
    img >>= gen = Image $ \x y -> getImage (gen $ getImage img x y) x y

-- Pointed Infinite
instance Comonad Image where
    extract img = getImage img 0 0
    duplicate img = Image $ \x y -> Image $ \i j -> getImage img (i-x) (j-y)

{--
instance (Enum a, Bounded a, Eq b) => Eq (a -> b) where
    f == g = and $ zipWith (==) (fmap f domain) (fmap g domain)
                where domain = enumFromTo minBound maxBound 
instance (Enum a, Bounded a, Ord b) => Ord (a -> b) where
    f <= g = and $ zipWith (<=) (fmap f domain) (fmap g domain)
                where domain = enumFromTo minBound maxBound 
instance Num b => Num (a -> b) where
    (+) = liftA2 (+)
    (-) = liftA2 (-)
    (*) = liftA2 (*)
    abs = fmap abs
    signum = fmap signum
    fromInteger = const . fromInteger

instance (Enum a, Bounded a, Integral b) => Integral (a -> b) where
instance Enum b => Enum (a -> b) where
instance (Enum a, Bounded a, Real b) => Real (a -> b) where
--}
